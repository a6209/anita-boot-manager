/**********************************************************************
 *  main.cpp
 **********************************************************************
 * Copyright (C) 2017 Anita
 *
 * Authors: Adrian, Dolphin Oracle
 *          Anita <http://avahi_daemon.gitlab.io>
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this package. If not, see <http://www.gnu.org/licenses/>.
 **********************************************************************/


#include "mainwindow.h"
#include <unistd.h>
#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QIcon>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon::fromTheme("garuda-boot-options"));

    QTranslator qtTran;
    qtTran.load(QString("qt_") + QLocale::system().name());
    a.installTranslator(&qtTran);

    QTranslator appTran;
    appTran.load(QString("garuda-boot-options_") + QLocale::system().name(), "/usr/share/garuda-boot-options/locale");
    a.installTranslator(&appTran);

    if (getuid() == 0)
    {
        if (system("mountpoint -q /live/aufs") == 0) {
            QApplication::beep();
            QMessageBox::critical(0, QString::null,
                                  QApplication::tr("This programs is not meant to run in a live environment."));
            return 1;
        }
        MainWindow w;
        w.show();
        return a.exec();
    } else {
        QApplication::beep();
        QMessageBox::critical(0, QString::null,
                              QApplication::tr("You must run this program as root."));
        return 1;
    }
}
